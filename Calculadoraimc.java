import java.util.Scanner;

public class Calculadoraimc {
    public static void main(String [] args) {
        System.out.println(" ** CALCULADORA DE INDICE DE MASSA CORPORAL (IMC) =====");
        
        Scanner scanner = new Scanner (System.in);
        System.out.print("Informe o seu PESO em kilos: ");
                double numero1 = scanner.nextDouble();

        System.out.print("Informe a sua ALTURA em metros: ");
                double numero2 = scanner.nextDouble();
                double multiplicacao = numero2 * numero2;
                double divisao = numero1 / multiplicacao;
        System.out.println("=========================================================");
        System.out.println("Seu Indice de Massa Corporal (IMC) é: " + divisao);
        System.out.println("=========================================================");
        System.out.println("Sua condicao é:");

        if (divisao >= 18.5 && divisao <= 24.9) {
            System.out.println("NORMAL ");
        } else if (divisao <18.5) {
            System.out.println("MAGREZA !");
        } else if (divisao >= 25.0 && divisao <= 29.9) {
            System.out.println("SOBRE PESO !");
            double subtracao = divisao - 24.9;
            System.out.println("Voce deve reduzir o seu peso em pelo menos: " + subtracao + " Kg");
        } else if (divisao >= 30.0) {
            System.out.println("OBESIDADE !!!");
            double subtracao = divisao - 24.9;
            System.out.println("Voce deve reduzir o seu peso em pelo menos: " + subtracao + " Kg");

        }
    }
// Incluindo linha para o Exercicio Modulo 3
}
